export default [
  {
    name: 'Application Layer',
    threats: [
      'Pushing',
      'Malware injection',
      'DDos attacks',
    ]
  },
  {
    name: 'Presentation Layer',
    threats: [
      'Encoding/decoding vulnerabilities',
      'Format string attacks',
      'Malicious code injection',
    ]
  },
  {
    name: 'Session Layer',
    threats: [
      'Session hijacking',
      'Session fixation attacks',
      'Brute force attacks',
    ]
  },
  {
    name: 'Transport Layer',
    threats: [
      'Man-in-the-middle attacks',
      'SYN/ACK flood',
    ]
  },
  {
    name: 'Network Layer',
    threats: [
      'IP spoofing',
      'Route table manipulation',
      'DDos attacks',
    ]
  },
  {
    name: 'Data Link Layer',
    threats: [
      'MAC address spoofing',
      'ARP spoofing',
      'VLAN hopping',
    ]
  },
  {
    name: 'Physical Layer',
    threats: [
      'Wiretapping',
      'Physical tampering',
      'Electromagnetic interference',
    ]
  },
]