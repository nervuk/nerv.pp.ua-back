export default [
  {
    name: 'Docker Image',
    route: 'docker/image',
    data: [
      {
        name: '1. Build an image from a Dockerfile:',
        examples: [{
          command: 'docker build -t image_name path_to_dockerfile',
          example: 'docker build -t myapp .'
        }],
      },
      {
        name: '2. List all local images:',
        examples: [{
          command: 'docker images',
          example: 'docker image ls'
        }],
      },
      {
        name: '3. Pull an image from Docker Hub:',
        examples: [{
          command: 'docker pull image_name:tag',
          example: 'docker pull nginx:latest'
        }],
      },
      {
        name: '4. Remove a local image:',
        examples: [{
          command: 'docker rmi image_name:tag',
          example: 'docker rmi myapp:latest'
        }],
      },
      {
        name: '4. Remove a container:',
        examples: [{
          command: 'docker rm [image_name/image_id]',
          example: 'docker rm fd484f19954f'
        }],
      },
      {
        name: '5. Tag an image:',
        examples: [{
          command: 'docker tag source_image:tag new_image:tag',
          example: 'docker tag myapp:latest myapp:v1'
        }],
      },
      {
        name: '6. Push an image to Docker Hub:',
        examples: [{
          command: 'docker push image_name:tag',
          example: 'docker push myapp:v1'
        }],
      },
      {
        name: '7. Inspect details of an image:',
        examples: [{
          command: 'docker inspect image_name:tag',
          example: 'docker inspect myapp:v1'
        }],
      },
      {
        name: '8. Save an image to a tar archive:',
        examples: [{
          command: 'docker save -o image_name.tar image_name:tag',
          example: 'docker save -o myapp.tar myapp:v1'
        }],
      },
      {
        name: '9. Load an image from a tar archive:',
        examples: [{
          command: 'docker -iload image_name.tar',
          example: 'docker -iload image_name.tar'
        }],
      },
      {
        name: '10. Prune unused images:',
        examples: [{
          command: 'docker image prune',
          example: ''
        }],
      },
    ]
  },
  {
    name: 'Docker Container',
    route: 'docker/container',
    data: [
      {
        name: '1. Run a container from an image:',
        examples: [{
          command: 'docker run container_name image_name',
          example: 'docker run myapp'
        }],
      },
      {
        name: '2. Run a named container from an image:',
        examples: [{
          command: 'docker run --name container_name image_name:tag',
          example: 'docker run --name my_container myapp:v1'
        }],
      },
      {
        name: '3. List all running containers:',
        examples: [{
          command: 'docker ps',
          example: ''
        }],
      },
      {
        name: '4. List all containers (including stopped ones):',
        examples: [{
          command: 'docker ps -a',
          example: ''
        }],
      },
      {
        name: '5. Stop a running container:',
        examples: [{
          command: 'docker stop container_name_or_id',
          example: 'docker stop my_container'
        }],
      },
      {
        name: '6. Start a stopped container:',
        examples: [{
          command: 'docker start container_name_or_id',
          example: 'docker start my_container'
        }],
      },
      {
        name: '7. Run container in interactive mode:',
        examples: [{
          command: 'docker run -it container_name_or_id',
          example: 'docker run -it my_container'
        }],
      },
      {
        name: '8. Run container in interactive shell mode',
        examples: [{
          command: 'docker run -it container_name_or_id sh',
          example: 'docker run -it my_container sh'
        }],
      },
      {
        name: '9. Remove a stopped container:',
        examples: [{
          command: 'docker rm container_name_or_id',
          example: 'docker rm my_container'
        }],
      },
      {
        name: '10. Remove a running container (forcefully):',
        examples: [{
          command: 'docker rm -f container_name_or_id',
          example: 'docker rm -f my_container'
        }],
      },
      {
        name: '11. Inspect details of a container:',
        examples: [{
          command: 'docker inspect container_name_or_id',
          example: 'docker inspect my_container',
        }],
      },
      {
        name: '12. View container logs: ',
        examples: [{
          command: 'docker logs container_name_or_id',
          example: 'docker logs my_container',
        }],
      },
      {
        name: '13. Pause a running container:',
        examples: [{
          command: 'docker pause container_name_or_id',
          example: 'docker pause my_container',
        }],
      },
      {
        name: '14. Unpause a paused container:',
        examples: [{
          command: 'docker unpause container_name_or_id',
          example: 'docker unpause my_container',
        }],
      },
    ]
  },
  {
    name: 'Docker Volumes and Network',
    route: 'docker/vol-net',
    data: [
      {
        name: '1. Create a named volume:',
        examples: [{
          command: 'docker volume create volume_name',
          example: 'docker volume create my_volume',
        }],
      },
      {
        name: '2. List all volumes:',
        examples: [{
          command: 'docker volume ls',
          example: '',
        }],
      },
      {
        name: '3. Inspect details of a volume:',
        examples: [{
          command: 'docker volume inspect volume_name',
          example: 'docker volume inspect my_volume',
        }],
      },
      {
        name: '4. Remove a volume:',
        examples: [{
          command: 'docker volume rm volume_name',
          example: 'docker volume rm my_volume',
        }],
      },
      {
        name: '5. Run a container with a volume (mount):',
        examples: [{
          command: 'docker run --name container_name -v volume_name:/path/in/container image_name:tag',
          example: 'docker run --name my_container -v my_volume:/app/data myapp:v1',
        }],
      },
      {
        name: '6. Copy files between a container and a volume:',
        examples: [{
          command: 'docker cp local_file_or_directory container_name:/path/in/container',
          example: 'docker cp data.txt my_container:/app/data',
        }],
      },
    ]
  },
  {
    name: 'Docker Volumes and Network / Network (Port Mapping):',
    route: 'docker/vol-net',
    data: [
      {
        name: '1. Run a container with port mapping:',
        examples: [{
          command: 'docker run --name container_name -p host_port:container_port image_name',
          example: 'docker run --name my_container -p 8080:80 myapp',
        }],
      },
      {
        name: '2. List all networks:',
        examples: [{
          command: 'docker network ls',
          example: '',
        }],
      },
      {
        name: '3. Inspect details of a network:',
        examples: [{
          command: 'docker network inspect network_name',
          example: 'docker network inspect bridge',
        }],
      },
      {
        name: '4. Create a user-defined bridge network:',
        examples: [{
          command: 'docker network create network_name',
          example: 'docker network create my_network',
        }],
      },
      {
        name: '5. Connect a container to a network:',
        examples: [{
          command: 'docker network connect network_name container_name',
          example: 'docker network connect my_network my_container',
        }],
      },
      {
        name: '6. Disconnect a container from a network:',
        examples: [{
          command: 'docker network disconnect network_name container_name',
          example: 'docker network disconnect my_network my_container',
        }],
      },
    ]
  },
  {
    name: 'Docker Compose',
    route: 'docker/compose',
    data: [
      {
        name: '1. Create and start containers defined in a docker-compose.yml file:',
        examples: [{
          command: 'docker compose up',
          example: '',
        }],
      },
      {
        name: '2. Stop and remove containers defined in a docker-compose.yml file:',
        examples: [{
          command: 'docker compose down',
          example: '',
        }],
      },
      {
        name: 'Build or rebuild services:',
        examples: [{
          command: 'docker compose build',
          example: '',
        }],
      },
      {
        name: '4. List containers for a specific Docker Compose project:',
        examples: [{
          command: 'docker compose ps',
          example: '',
        }],
      },
      {
        name: '5. View logs for services:',
        examples: [{
          command: 'docker compose logs',
          example: '',
        }],
      },
      {
        name: '6. Scale services to a specific number of containers:',
        examples: [{
          command: 'docker compose up -d --scale service_name=number_of_containers',
          example: 'docker compose up -d --scale web=3',
        }],
      },
      {
        name: '7. Run a one-time command in a service:',
        examples: [{
          command: 'docker compose run service_name command',
          example: 'docker compose run web npm install',
        }],
      },
      {
        name: '8. List all volumes:',
        examples: [{
          command: 'docker volume ls',
          example: '',
        }],
      },
      {
        name: '9. Pause a service:',
        examples: [{
          command: 'docker volume pause service_name',
          example: '',
        }],
      },
      {
        name: '10. Unpause a service:',
        examples: [{
          command: 'docker volume unpause service_name',
          example: '',
        }],
      },
      {
        name: '11. View details of a service:',
        examples: [{
          command: 'docker compose ps service_name',
          example: '',
        }],
      },
    ]
  },
  {
    name: 'Latest Docker',
    route: 'docker/latest',
    data: [
      {
        name: '1. Initialize Docker inside an application',
        examples: [{
          command: 'docker init',
          example: '',
        }],
      },
      {
        name: '2. Watch the service/container of an application',
        examples: [{
          command: 'docker compose watch',
          example: '',
        }],
      },
    ]
  },
  {
    name: 'Dockerfile Reference',
    route: 'docker/dockerfile',
    data: [
      {
        name: 'FROM:',
        examples: [{
          command: 'FROM image_name:tag',
          example: 'FROM ubuntu:20.04',
        }],
      },
      {
        name: 'WORKDIR:',
        examples: [{
          command: 'WORKDIR /path/to/directory',
          example: 'WORKDIR /app',
        }],
      },
      {
        name: 'COPY:',
        examples: [{
          command: 'COPY host_source_path container_destination_path',
          example: 'COPY . .',
        }],
      },
      {
        name: 'RUN:',
        examples: [{
          command: 'RUN command1 && command2',
          example: 'RUN apt-get update && apt-get install -y curl',
        }],
      },
      {
        name: 'ENV:',
        examples: [{
          command: 'ENV KEY=VALUE',
          example: 'ENV NODE_VERSION=14',
        }],
      },
      {
        name: 'EXPOSE:',
        examples: [{
          command: 'EXPOSE port',
          example: 'EXPOSE 8080',
        }],
      },
      {
        name: 'CMD:',
        examples: [
          {
            command: 'CMD ["executable","param1","param2"]',
            example: 'CMD ["npm", "start"]',
          },
          {
            name: '',
            command: 'CMD executable param1 param2',
            example: 'CMD npm run dev',
          },
        ],
      },
      {
        name: 'ENTRYPOINT:',
        examples: [
          {
            command: 'ENTRYPOINT ["executable","param1","param2"]',
            example: 'ENTRYPOINT ["node", "app.js"]',
          },
          {
            command: 'ENTRYPOINT executable param1 param2',
            example: 'ENTRYPOINT node app.js',
          },
        ]
      },
      {
        name: 'ARG:',
        examples: [
          {
            command: 'ARG VARIABLE_NAME=default_value',
            example: 'ARG VERSION=latest',
          },
        ]
      },
      {
        name: 'VOLUME:',
        examples: [
          {
            command: 'VOLUME /path/to/volume',
            example: 'VOLUME /data',
          },
        ]
      },
      {
        name: 'LABEL:',
        examples: [
          {
            command: 'LABEL key="value"',
            example: 'LABEL version="1.0" maintainer="Adrian"',
          },
        ]
      },
      {
        name: 'USER:',
        examples: [
          {
            command: 'USER user_name',
            example: 'USER app',
          },
        ]
      },
      {
        name: 'ADD:',
        examples: [
          {
            command: 'ADD source_path destination_path',
            example: 'ADD ./app.tar.gz /app',
          },
        ]
      },
      {
        name: 'Dockerfile',
        file_example: '# Use an official Node.js runtime as a base image\n\nFROM node:20-alpine\n\n\n\n# Set the working directory to /app\n\nWORKDIR /app\n\n\n\n# Copy package.json and package-lock.json to the\nworking directory\n\nCOPY package*.json ./\n\n\n\n# Install dependencies\n\nRUN npm install\n\n\n\n# Copy the current directory contents to the container\nat /app\n\nCOPY . .\n\n\n\n# Expose port 8080 to the outside world\n\nEXPOSE 8080\n\n\n\n# Define environment variable\n\nENV NODE_ENV=production\n\n\n\n# Run app.js when the container launches\n\nCMD node app.js\n',
      },
    ]
  },
  {
    name: 'Docker Compose File Reference',
    route: 'docker/composefile',
    data: [
      {
        name: 'version:',
        examples: [
          {
            command: 'version: \'3.8\'',
            example: '',
          },
        ]
      },
      {
        name: '',
        examples: [
          {
            command: 'services :\n web :  \nimage : nginx:latest',
            example: '',
          },
        ]
      },
      {
        name: 'networks:',
        examples: [
          {
            command: 'networks :\n            my_network : \n                        driver : bridge',
            example: '',
          },
        ]
      },
      {
        name: 'volumes:',
        examples: [
          {
            command: 'volumes :\n            my_volume :',
            example: '',
          },
        ]
      },
      {
        name: 'environment:',
        examples: [
          {
            command: 'environment:\n            - NODE_ENV=production',
            example: '',
          },
        ]
      },
      {
        name: 'ports:',
        examples: [
          {
            command: 'ports:\n            - "8080:80"',
            example: '',
          },
        ]
      },
      {
        name: 'depends_on:',
        examples: [
          {
            command: 'depends_on :\n            - db',
            example: '',
          },
        ]
      },
      {
        name: 'build:',
        examples: [
          {
            command: 'build :\n            context : . \n                        dockerfile : Dockerfile.dev',
            example: '',
          },
        ]
      },
      {
        name: 'volumes_from:',
        examples: [
          {
            command: 'volumes_from :\n            - service_name',
            example: '',
          },
        ]
      },
      {
        name: 'command:',
        examples: [
          {
            command: 'command : ["npm", "start"]',
            example: '',
          },
        ]
      },
      {
        name: 'Docker Compose file',
        file_example: 'version: \'3.8\'\n\n\n# Define services for the MERN stack\n\nservices : \n\n\n\n# MongoDB service\n\nmongo : \n\nimage : mongo:latest\n\nports :\n\n- "27017:27017"\n\nvolumes :\n\n- mongo_data : /data/db\n\nenvironment :\n\nMONGO_INITDB_ROOT_USERNAME : admin \n\nMONGO_INITDB_ROOT_PASSWORD : admin \n\n\n\n# Node.js (Express) API service\n\napi :\n\nbuild :\n\n# Specify the build context for the API service\n\ncontext : ./api\n\n\n# Specify the Dockerfile for building the API service\ndockerfile : Dockerfile\n\nports :\n\n- "5000:5000" \n\n\n\n# Ensure the MongoDB service is running before starting\nthe API\n\ndepends_on :\n\n- mongo\n\n\n\n\nenvironment :\n\nMONGO_URI : mongodb://admin:admin@mongo:27017/\nmydatabase\n\n\n\nnetworks :\n\n- mern_network \n\n\n\n# React client service\n\nclient :\n\nbuild :\n\n# Specify the build context for the client service\n\ncontext : ./client \n\n# Specify the Dockerfile for building the client service\n\n\ndockerfile : Dockerfile\n\n\nports :\n\n- "3000:3000"\n\n# Ensure the API service is running before starting the\nclient\n\ndepends_on :\n\n- api\n\n\n\nnetworks :\n\n- mern_network \n\n\n# Define named volumes for persistent data\n\nvolumes :\n\nmongo_data :\n\n\n# Define a custom network for communication between\nservices\n\nnetworks :\n\nmern_network :\n',
      },
    ],
  },
]
