import docker from './docker';
import osi from './security/network/osi';
import auth from './security/auth';
import encription from './security/encryption';

export default [
  {
    name: 'Docker',
    catalogue: docker
  },
  {
    name: 'Security',
    catalogue: [
      {
        name: 'network',
        catalogue: [
          {
            name: 'osi',
            catalogue: osi,
          },
        ]
      },
      {
        name: 'Authentication / Authorization Methods',
        catalogue: auth,
      },
      {
        name: 'Encription Methods',
        catalogue: encription,
      },
    ],
  },
]
