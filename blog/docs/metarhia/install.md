[Index](./index.md)

# Installation

## Installation from scratch

**Dependencies:**
- impress (server core)
- metasql (for work with sql servers)
- pg (work with postgres server)
- redis (work with redis server)

**Developer Dependencies:**

- TypeScript:
  - typescript
  - @types/node
  - @types/pg
  - @types/ws

- Eslint
  - eslint
  - eslint-config-metarhia
  - eslint-config-prettier
  - eslint-plugin-import
  - eslint-plugin-prettier

- Prettier
  - prettier


> **Warning:** &#9888; Don't use `pnpm` package manager because error.

```
$ npm init
$ npm i impress metasql
$ npm i pg
$ npm i redis
$ npm i --save-dev typescript @types/node @types/pg @types/ws 
$ npm i --save-dev eslint eslint-config-metarhia eslint-config-prettier eslint-plugin-import eslint-plugin-prettier
$ npm i --save-dev prettier
```
 
### Create directories and configuration files

Create directory config

```
$ mkdir config
$ cd config
$ touch database.js log.js scale.js server.js sessions.js
```
#### Mandatory

Examples: 

- [database.js](./config/database.md)
- [log.js](./config/log.md)
- [scale.js](./config/scale.md)
- [server.js](./config/server.md)
- [sessions.js](./config/sessions.md)


#### Additional

Examples: 

- [cache.js](./config/cache.md)
- [examples.js](./config/examples.md)
- [log.debug.js](./config/log.debug.md)
- [log.test.js](./config/log.test.md)
- [remote.js](./config/remote.md)
- [resmon.js](./config/resmon.md)


## Installation from example [<img alt="GitHub" width="30px" src="../../../public/pic/logos/github-mark.png" />](https://github.com/metarhia/Example/tree/master)

```
$ git clone git@github.com:metarhia/Example.git
$ git clone https://github.com/metarhia/Example.git
```