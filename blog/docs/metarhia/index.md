# Metarhia

Metarhia is the first technology stack for Node.js scaled with threads, offering ultra-lightweight workload isolation. It is optimized for high-intensive data exchange, rapid development, clean architecture, and easily understandable domain-specific code. Metarhia provides an all-in-one solution for a reliable and efficient backend. It supports network communication with web and mobile clients, offers a protocol-agnostic API, run-time type validation, real-time and in-memory data processing, and both stateful and stateless services.

- [Official Documentation](https://github.com/metarhia/Docs)
- [Metarhia specification](https://github.com/metarhia/Contracts)
- [Specification of Metarhia Distributed Infrastructure](https://github.com/metarhia/Infrastructure)
- [Roadmap](https://github.com/metarhia/Roadmap)
- [Metarhia best practice for relative technologies](https://github.com/metarhia/BestPractice)
- [Metarhia application minimal template](https://github.com/metarhia/Template)
- [Metarhia Community Manifesto](https://github.com/metarhia/Manifesto)
- [Youtube](https://www.youtube.com/@Metarhia)
- [Web-site metarhia.com (sources)](https://github.com/metarhia/metarhia.com)
- [Partners Metarhia (NodeUA and HowProgrammingWorks) Community Partners](https://github.com/metarhia/Partners)

## My Experience

- [Installation](./install.md)
- [Strucuture of Directories](./dirs.md)

## Modules

| # | Module | Description |
|-|-|-|
| 1 | [Impess](./impress.md) | Impress Application Server for Node.js and Metarhia Technology Stack |
| 2 | [Metautil](./metautil/index.md) | Metarhia utilities |
| 3 | [metavm](https://github.com/metarhia/metavm) | Metarhia script loader, node.js vm wrapper|
| 4 | [sql](https://github.com/metarhia/sql) | Interface: sql|
| 5 | [metasync](https://github.com/metarhia/metasync) | Asynchronous Programming Library |
| 6 | [do](https://github.com/metarhia/do) | "do" is the simplest way to manage asynchronicity |
| 7 | [metasql](https://github.com/metarhia/metasql) | MetaSQL schema generator & access layer |
| 8 | [metacom](https://github.com/metarhia/metacom) | Metacom Communication Protocol for Metarhia |
| 9 | [Concolor](https://github.com/metarhia/concolor) | Concolor is a simple library for Node.js for coloring templated strings using tags with annotations |
| 10 | [metawatch](https://github.com/metarhia/metawatch) | Deep nested directories watch for node.js |
| 11 | [noroutine](https://github.com/metarhia/noroutine) | Node Routine (noroutine) |
| 12 | [metalog](https://github.com/metarhia/metalog) | Meta Logger for Metarhia |
| 13 | [Metaschema](https://github.com/metarhia/metaschema) | Metadata schema and interface (contract) definition language |
| 14 | [metaconfiguration](https://github.com/metarhia/metaconfiguration) | Metarhia Configuration Loader |
| 15 | [eslint-config-metarhia](https://github.com/metarhia/eslint-config-metarhia) | Opinionated ESLint config and de-facto JavaScript styleguide for Metarhia projects. |
| 16 | [web-locks ](https://github.com/metarhia/web-locks) | Web Locks API |
| 17 | [tickplate ](https://github.com/metarhia/tickplate) | Tickplate - Back-tick templates for JavaScript |
| 18 | [jstp](https://github.com/metarhia/JSTP) | JSTP / JavaScript Transfer Protocol |
| 19 | [globalstorage](https://github.com/metarhia/globalstorage/tree/master) | Global Storage is a Global Distributed Data Warehouse |
| 20 | [metatests](https://github.com/metarhia/metatests) | metatests is an extremely simple to use test framework and runner for Metarhia technology  |
| 21 | [common](https://github.com/metarhia/common) | Metarhia Common Library |
| 22 | [metadomain ](https://github.com/metarhia/metadomain/tree/main) | Metadomain is Metarhia core database model |
| 23 | [sandboxed-fs](https://github.com/metarhia/sandboxed-fs) | sandboxed-fs is a sandboxed wrapper for Node.js file system module implementing the same API but bound to a certain directory, reliably locked in it. |
| 24 | [metatests-browser-runner](https://github.com/metarhia/metatests-browser-runner) | Browser test runner for metatests |
| 25 | [highcode](https://github.com/metarhia/highcode) | High-code is like low-code and no-code, but with higher abstraction  |
| 26 | [metascheduler](https://github.com/metarhia/metascheduler/tree/main) | Metarhia task scheduler |
| 27 | [eslint-plugin-impress](https://github.com/metarhia/eslint-plugin-impress) | ESLint plugin for Impress Application Server itself and any applications built with Impress. |
| 28 | [metadoc ](https://github.com/metarhia/metadoc/blob/master/package.json) | Metarhia Documentation Generator |
| 29 | [filestorage](https://github.com/metarhia/filestorage) | FileStorage is a library that allows to easily store a large number of binary files. |
| 30 | [metastreams](https://github.com/metarhia/metastreams) | Readable and Writable Streams with buffering |
| 31 | [remark-preset-lint-metarhia](https://github.com/metarhia/remark-preset-lint-metarhia) | Opinionated remark-lint preset, originated from the config used in metarhia-jstp and decoupled into a separate and shareable one. |
| 32 | [tools](https://github.com/metarhia/tools/blob/master/package.json) | Metarhia development tools |
| 33 | [mdsf](https://github.com/metarhia/mdsf) | Metarhia Data Serialization Format |
| 34 | [iterator](https://github.com/metarhia/iterator) | efficient and composable iteration |
| 35 | [metamail](https://github.com/metarhia/metamail) | Metarhia mail subsystem |
| 36 | [metastorage ](https://github.com/metarhia/metastorage) | metastorage: Metarhia common types |
| 37 | [lowscript](https://github.com/metarhia/lowscript/blob/main/package.json) | Lowcode is a limited subset of JavaScript for low-code with Metarhia technology stack |
| 38 | [mtypes](https://github.com/metarhia/mtypes) | Mtypes: Metarhia common types |
| 39 | [metagui](https://github.com/metarhia/metagui/blob/main/package.json) | UI Components for Metarhia technology stack |
| 40 | [metagram](https://github.com/metarhia/metagram/tree/main) | ERD generator for metaschema |
| 41 | [protocol](https://github.com/metarhia/protocol) | Metarhia Protocol |
| 42 | [namespace](https://github.com/metarhia/namespace/tree/master) | High stable namespace registry |
| 43 | [jstp-java](https://github.com/metarhia/jstp-java) | Java JSTP implementation |
| 44 | [jstp-swift ](https://github.com/metarhia/jstp-swift) | JavaScript Transfer Protocol |

## Examples

| # | Module | Description |
|-|-|-|
| 1 | [Example](https://github.com/metarhia/Example)|  |
| 2 | [MetaTalk Example](https://github.com/metarhia/MetaTalk)|  |
| 3 | [metacalc (metavm Example)](https://github.com/metarhia/metacalc/tree/main)|  |
| 4 | [Example Web Site for metacms](https://github.com/metarhia/ExampleWebSite)|  |
| 5 | [xxii](https://github.com/metarhia/xxii/tree/main) | XXII News |
| 6 | [impress-example](https://github.com/metarhia/impress-example) | Example application for Impress Application Server |
| 7 | [JSQL](https://github.com/metarhia/JSQL) | JSQL / JavaScript Query Language |
| 8 | [messenger](https://github.com/metarhia/messenger) | Metarhia Messenger |

## Other

- [Metarhia CMS](https://github.com/metarhia/metacms)
- [Arhaica Content Management System](https://github.com/metarhia/arhaica/blob/master/package.json)
- [console-web](https://github.com/metarhia/console-web) - Metarhia web client
- [metacom-android ](https://github.com/metarhia/metacom-android) - MetaCom is a very secure instant messenger and file exchange tool. Both messaging and file transactions are anonymous and private.

### Back-end

- [metacommand](https://github.com/metarhia/metacommand/tree/master) - Metarhia command-line interface utilities

### Front-end

- [swayer](https://github.com/rohiievych/swayer) - Swayer - schema based UI engine
- [B-OS Demo Application](https://github.com/leonpolak/Demo) 

## Clear repositories

- [Metalocal](https://github.com/metarhia/Metalocal)
- [Accounts](User accounts, contacts, permissions, and authentication subsystem 👥)
- [Metapay](Metarhia payment subsystem 🪙)
- [Metanet](https://github.com/metarhia/Metanet) - Communication, messaging, file transfer and content publishing
- [ impress-cli](https://github.com/metarhia/impress-cli) - This module is a part of Impress Application Server for node.js. This module is temporarily frozen but will be rewritten after Impress 2.0 release.
- [thread-balancer](https://github.com/metarhia/thread-balancer)
- [prettier-config ](https://github.com/metarhia/prettier-config)
- [metaschema-langserver](https://github.com/metarhia/metaschema-langserver) - Metaschema Language Server Protocol
- [metacode ](https://github.com/metarhia/metacode)










