[Back](../install.md)

# log.test.js

```javascript
({
  keepDays: 1,
  writeInterval: 1000,
  writeBuffer: 64 * 1024,
  toFile: ['error', 'warn', 'info', 'debug', 'log'],
  toStdout: ['error', 'warn', 'info', 'debug', 'log'],
});
```