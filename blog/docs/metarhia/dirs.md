[Index](./index.md)

# Strucuture of Directories

- &#128193; application
  - &#128193; api
    - &#128193; auth
    - &#128193; chat
    - &#128193; console
    - &#128193; example
    - &#128193; files
    - &#128193; system
    - &#128193; test
    - &#128196; geo.1.js
    - &#128196; hook.1.js
  - &#128193; bus
  - &#128193; cert
  - &#128193; config
  - &#128193; db
  - &#128193; domain
  - &#128193; lib
  - &#128193; resources
  - &#128193; schemas
  - &#128193; static
  - &#128193; tasks
- &#128193; log






