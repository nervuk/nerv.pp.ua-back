[Index](./index.md)

# Impress

## Strucuture of Directories

- &#128193; lib
  - &#128196; api.js
  - &#128196; application.js
  - &#128196; auth.js
  - &#128196; bus.js
  - &#128196; cert.js
  - &#128196; code.js
  - &#128196; deps.js
  - &#128196; place.js
  - &#128196; planner.js
  - &#128196; procedure.js
  - &#128196; reporter.js
  - &#128196; scheduler.js
  - &#128196; schemas.js
  - &#128196; static.js
  - &#128196; storage.js
  - &#128196; worker.js

- &#128193; schemas
  - &#128193; config
    - &#128196; .types.js
    - &#128196; cache.js
    - &#128196; log.js
    - &#128196; scale.js
    - &#128196; server.js
    - &#128196; sessions.js

  - &#128193; contracts
    - &#128196; procedure.js

- &#128193; test
- &#128193; types
  - &#128196; config.d.ts
  - &#128196; core.d.ts
  - &#128196; impress.d.ts
  - &#128196; procedure.d.ts
  - &#128196; tsconfig.json