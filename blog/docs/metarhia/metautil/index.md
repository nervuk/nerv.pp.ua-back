[Index](../index.md)

# Metautil [5.2.0] [<img alt="alt_text" width="30px" src="../../../../public/pic/logos/github-mark.png" />](https://github.com/metarhia/metautil/tree/master)

## Async utilities

- toBool = [() =&gt; true, () =&gt; false] [&#128221;](./examples/tobool.md)
- timeout(msec: number, signal?: AbortSignal): Promise&lt;void&gt;
- delay(msec: number, signal?: AbortSignal): Promise&lt;void&gt;
- timeoutify(promise: Promise&lt;unknown&gt;, msec: number): Promise&lt;unknown&gt;
- collect(keys: Array&lt;string&gt;, options?: CollectorOptions): Collector
  - options.exact?: boolean
  - options.timeout?: number
  - options.reassign?: boolean


## Class Collector [&#128221;](./examples/collector.md)

- new Collector(keys: Array&lt;string&gt;, options?: CollectorOptions)
  - options.exact?: boolean
  - options.timeout?: number
  - options.reassign?: boolean
- set(key: string, value: unknown)
- wait(key: string, fn: AsyncFunction | Promise&lt;unknown&gt;, ...args?: Array&lt;unknown&gt;)
- take(key: string, fn: Function, ...args?: Array&lt;unknown&gt;)
- collect(sources: Record&lt;string, Collector&gt;)
- fail(error: Error)
- then(fulfill: Function, reject?: Function)
- done: boolean
- data: Dictionary
- keys: Array&lt;string&gt;
- count: number
- exact: boolean
- timeout: number

## Crypto utilities

- cryptoRandom(min?: number, max?: number): number
- random(min?: number, max?: number): number
- generateUUID(): string
- generateKey(possible: string, length: number): string
- crcToken(secret: string, key: string): string
- generateToken(secret: string, characters: string, length: number): string
- validateToken(secret: string, token: string): boolean
- serializeHash(hash: Buffer, salt: Buffer): string
- deserializeHash(phcString: string): HashInfo
- hashPassword(password: string): Promise&lt;string&gt;
- validatePassword(password: string, serHash: string): Promise&lt;boolean&gt;
- md5(fileName: string): Promise&lt;string&gt;
- getX509(cert: X509Certificate): Strings [&#128221;](./examples/x509.md)


## Datetime utilities

- duration(s: string | number): number
- nowDate(date?: Date): string
- nowDateTimeUTC(date?: Date, timeSep?: string): string
- parseMonth(s: string): number
- parseDay(s: string): number
- parseEvery(s: string): Every
- nextEvent(every: Every, date?: Date): number

## Error utilities

### Class Error

- new Error(message: string, options?: number | string | ErrorOptions)
    options.code?: number | string
    options.cause?: Error
  - message: string
  - stack: string
  - code?: number | string
  - cause?: Error

### Class DomainError
- new DomainError(code?: string, options?: number | string | ErrorOptions)
  - options.code?: number | string
  - options.cause?: Error
- message: string
- stack: string
- code?: number | string
- cause?: Error
- toError(errors: Errors): Error

### Functions

- isError(instance: object): boolean


## File system utilities

- directoryExists(path: string): Promise&lt;boolean&gt;
- ensureDirectory(path: string): Promise&lt;boolean&gt;
- parsePath(relPath: string): Strings

## HTTP utilities

- parseHost(host?: string): string
- parseParams(params: string): Cookies
- parseCookies(cookie: string): Headers
- parseRange(range: string): StreamRange

## Network utilities

- Deprecated in 4.x: fetch(url: string, options?: FetchOptions): Promise&lt;Response&gt;
- receiveBody(stream: IncomingMessage): Promise&lt;Buffer | null&gt;
- ipToInt(ip?: string): number
- intToIp(int: number): string
- httpApiCall(url: string, options: ApiOptions): Promise&lt;object&gt;
  - options.method?: HttpMethod
  - options.headers?: object
  - options.body?: Body


## Objects utilities

- makePrivate(instance: object): object
- protect(allowMixins: Strings, ...namespaces: Namespaces): void
- jsonParse(buffer: Buffer): Dictionary | null
- isHashObject(o: string | number | boolean | object): boolean
- flatObject(source: Dictionary, fields: Strings): Dictionary
- unflatObject(source: Dictionary, fields: Strings): Dictionary
- getSignature(method: Function): Strings
- namespaceByPath(namespace: Dictionary, path: string): Dictionary | null
- serializeArguments(fields: Strings, args: Dictionary): string

## Class Pool [&#128221;](./examples/pool.md)

- new Pool(options: PoolOptions)
  - options.timeout?: number
- items: Array&lt;unknown&gt;
- free: Array&lt;boolean&gt;
- queue: Array&lt;unknown&gt;
- current: number
- size: number
- available: number
- timeout: number
- next(): Promise&lt;unknown&gt;
- add(item: unknown): void
- capture(): Promise&lt;unknown&gt;
- release(item: unknown): void
- isFree(item: unknown): boolean


## Array utilities

- sample(array: Array&lt;unknown&gt;): unknown [&#128221;](./examples/arr.md)
- shuffle(array: Array&lt;unknown&gt;): Array&lt;unknown&gt; [&#128221;](./examples/arr.md)
- projection(source: object, fields: Array&lt;string&gt;): Array&lt;unknown&gt; [&#128221;](./examples/arr.md)

## Class Semaphore [&#128221;](./examples/semaphore.md)

- new Semaphore(options: SemaphoreOptions)
  - options.concurrency: number
  - options.size?: number
  - options.timeout?: number
- concurrency: number
- counter: number
- timeout: number
- size: number
- empty: boolean
- queue: Array&lt;QueueElement&gt;
- enter(): Promise&lt;void&gt;
- leave(): void

## Strings utilities

- replace(str: string, substr: string, newstr: string): string
- between(s: string, prefix: string, suffix: string): string
- split(s: string, separator: string): [string, string]
- isFirstUpper(s: string): boolean
- isFirstLower(s: string): boolean
- isFirstLetter(s: string): boolean
- toLowerCamel(s: string): string
- toUpperCamel(s: string): string
- toLower(s: string): string
- toCamel(separator: string): (s: string) =&gt; string
- spinalToCamel(s: string): string
- snakeToCamel(s: string): string
- isConstant(s: string): boolean
- fileExt(fileName: string): string
- parsePath(relPath: string): Strings
- trimLines(s: string): string

## Units utilities [&#128221;](./examples/units.md)

- bytesToSize(bytes: number): string
- sizeToBytes(size: string): number

## Class EnevtEmitter

- getMaxListeners(): number
- listenerCount(name: string): number
- on(name: string, fn: Function)
- once(name: string, fn: Function)
- emit(name: string, ...args: Array&lt;unknown&gt;)
- remove(name: string, fn: Function)
- clear(name: string)

## EnevtEmitter utilities

- once(emitter: EventEmitter, name: string): Promise&lt;unknown&gt;[&#128221;](./examples/enevtemitter-utils.md)
