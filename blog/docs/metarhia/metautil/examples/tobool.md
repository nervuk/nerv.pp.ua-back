[Back](../index.md)

# toBool


**Example**

```javascript
const created = await mkdir(path).then(...toBool);
```
