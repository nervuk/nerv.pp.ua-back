[Back](../index.md)

# x509 (Examples)

```javascript
const x509 = new crypto.X509Certificate(cert);
const domains = metautil.getX509names(x509);
```