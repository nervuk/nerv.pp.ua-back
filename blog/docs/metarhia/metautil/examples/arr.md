[Back](../index.md)

# Array utilities (Examples)

sample

```javascript
const cards = ['🂡', '🃒', '🂮', '🂷', '🃚'];
const card = sample(cards);
```
shuffle

```javascript
const players = [{ id: 10 }, { id: 12 }, { id: 15 }];
const places = shuffle(players);
```

projection

```javascript
const player = { name: 'Marcus', score: 1500, socket };
const playerState = projection(player, ['name', 'score']);
```