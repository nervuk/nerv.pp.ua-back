[Back](../index.md)

# EnevtEmitter utilities (Examples)

```javascript
const ee = new metautil.EventEmitter();
setTimeout(() => ee.emit('name3', 'value'), 100);
const result = await metautil.once(ee, 'name3');
```