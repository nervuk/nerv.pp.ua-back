[Back](../index.md)

# Units utilities (Examples)

```javascript
const size = bytesToSize(100000000);
const bytes = sizeToBytes(size);
console.log({ size, bytes });
// { size: '100 MB', bytes: 100000000 }
```
|Symbol |	zeros |	Unit|
|-|-|-|
| yb |	24 | 	yottabyte |
| zb |	21 | 	zettabyte |
| eb |	18 | 	exabyte |
| pb |	15 | 	petabyte |
| tb |	12 | 	terabyte |
| gb |	9 	| gigabyte |
| mb |	6 	| megabyte |
| kb |	3 	| kilobyte |